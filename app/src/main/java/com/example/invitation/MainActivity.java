package com.example.invitation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btn;
    ListView listcon;

    List<Person> myArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.invito);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,InviteActivity.class);
                startActivity(intent);
            }
        });


        Intent i = getIntent();

        Person person = (Person)i.getSerializableExtra("contact");

        myArray.add("contact");

        ArrayAdapter<String>itemAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_expandable_list_item_1, myArray);

        listcon = findViewById(R.id.listman);

        listcon.setAdapter(itemAdapter);

    }
}
